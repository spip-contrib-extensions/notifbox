/**
 * ========================================
 * notifbox - notifications écran pour SPIP
 * ========================================
 */

class notifBox {

  /**
   * @param {Objet|String} message 
   * @param {Object} params 
   */
  constructor(message = null, params = {}) {

    // arguments[0] : soit Message texte, soit Tableau d'options
    if ( typeof arguments[0] == 'string') {
      params.message = message;
    } else if (typeof arguments[0] == 'object')  {
      params = message;
    }

    const settings = Object.assign({}, spipConfig.notifbox.params, params );

    this.title = settings.title;
    this.message = settings.message;
    this.footer = '';

    this.duration = parseInt(settings.duration) > 3000 ? parseInt(settings.duration) : 3000;
    this.timer = !!settings.timer;
    this.class = settings.class;

    this.interval;
    this.id = notifbox_queue.length;

    if (!!settings.raw) {

      this.output = !this.title ? '' : `<h3>${this.title}</h3><br>`;
      this.output += this.message;

    } else {

      this.output = `<div class="box pop ${this.class}">`;
      this.output += !this.title ? '' : `<div class="box__header clearfix"><h3>${this.title}</h3></div>`;
      this.output += !this.message ? '' : `<div class="box__body clearfix">${this.message}</div>`;
      if (!!this.timer) {
        this.output += `<div class="box__footer act clearfix">`;
        this.output += `<button id="removing-in-${this.id}" >${this.duration / 1000}&nbsp;s</button>`;
        this.output += `</div>`;
      }
      this.output += `</div>`; 

    }

    if (this.title || this.message) {
      this.create();
    }
    else {
      console.log('notifBox: [echec] il faut au moins un titre ou un message');
    }
  }

  create() {

    notifbox_queue.push(this);

    if (document.getElementById('notifbox-container') == null) {
      const node_container = document.createElement('div');
      node_container.id=`notifbox-container`;
      node_container.classList.add('notifbox-container');
      document.body.appendChild(node_container)
    }

    const node_item = document.createElement('div');
    node_item.id=`notifbox-${this.id}`;
    node_item.classList.add('notifbox-item');
    node_item.innerHTML += `
  <input type="checkbox" class="notifbox__close" id="close_notifbox-${this.id}"/>
  <label class="notifbox__close-label" for="close_notifbox-${this.id}"></label> 
  <div class="notifbox__content --animated">
    ${this.output}
    </div>
  </div>
    `;

    document.getElementById('notifbox-container').appendChild(node_item);

    document.getElementById(`close_notifbox-${this.id}`)
      .addEventListener(
        'change',
        handler => {
          if (handler.target.checked) {
            this.close()
          }
        },
        { once: true }
        );

    this.interval = setInterval(() => {
      this.duration -= 1000;
      const node_timer = document.getElementById(`removing-in-${this.id}`);
      if ( this.timer && node_timer ) {
        node_timer.innerHTML = `${this.duration / 1000}&nbsp;s`;
      }
    }, 1000);

    this.timeout = setTimeout(() => {
      this.close();
    }, this.duration);

  }

  close() {
    document.getElementById(`close_notifbox-${this.id}`).checked = true;
    clearInterval(this.interval);
    clearTimeout(this.timeout);
    setTimeout(() => {
      notifbox_queue.splice(this.id, 1, '');
      let node;
      if ( node = document.getElementById(`notifbox-${this.id}`)) node.remove();
    }, 1500);
  }

}


/***************************************************/
const notifbox_queue = [];

spipConfig.notifbox = Object.assign({
  params: {
    title: '',
    message: 'Message',  // il faut au moins renseigner title OU message
    duration: 8000,
    timer: false,        // afficher le timer ?
    class: 'info',       // simple|info|important|note|basic highlight|basic inverse|raccourcis|success|notice|error
    raw: false,          // permet d'opter pour un markup minimal, sans les div englobantes des box
  },
  selectors: {
    btn_static:'[data-notifbox]',
    url_action: '.action-notifbox',
    forms: '*:not(#charger_plugin_confirm) > [class^="reponse"], *:not(#charger_plugin_confirm) > [class*=" reponse"]', //transforme les message retours de formulaire en notif auto
    auto: '[data-notifbox-auto]',
  }
}, spipConfig.notifbox);

// des fonctions d'appel pour normaliser un peu les choses
const notifbox = function(_t) {
  return {
    log:  (_t) => {new notifBox(_t, {class:'info'})},
    ok:  (_t) => {new notifBox(_t, {class:'success'})},
    notice:  (_t) => {new notifBox(_t, {class:'notice'})},
    error:  (_t) => {new notifBox(_t, {class:'error',duration:60000})}
  }
}();

const notifbox_init = function() {

  function disableBtn (el) {
    el.setAttribute('aria-disabled', 'true');
    el.classList.add('btn_desactive');
  }

  function enableBtn (el) {
    el.removeAttribute('aria-disabled');
    el.classList.remove('btn_desactive');
  }

  function getSuffixDataAttributes (el, motif = 'data-') {
      return el.getAttributeNames().reduce((obj, name) => {
        if (name.startsWith(motif)) {
          return {...obj, [name.slice(name.lastIndexOf('-') + 1)]: el.getAttribute(name)};
        }
        return obj;
      }, {});  
  }

  function getValidConfigfromAttributes (el, motif = 'data-', ref = {}) {
    let config = {};
        Object.keys(getSuffixDataAttributes(el,motif))
          .filter( (e) => { return Object.keys(ref).includes(e)  })
          .map( (e) => { config[e] = el.getAttribute(motif+e) || ' '}) // data-notifbox-truc existe quoique vide = true !
    return config;
  }

  function fetchThenNotif(e)  {  
    e.preventDefault();
    e.stopPropagation();
    const el = e.target;
    const url = el.dataset.action || el.href;
    const btn_status = el.getAttribute('aria-disabled');

    if (url && !btn_status) {
      disableBtn(el);
      let config = getValidConfigfromAttributes(el,'data-notifbox-',spipConfig.notifbox.params);
        fetch(url)
          .then((response) => {
            if (!response.ok) {
              notif.notice('Impossible de charger la resssource'); // @todo lang
              enableBtn(el);
              throw new Error(`[notifbox : url_action]${response.status} ${response.statusText}`);
            }
            return (
              response.headers.get('Content-Type').indexOf('json') !== -1 // soit l'entete de la réponse 
            ) 
            ? response.json()
            : response.text()
          })
          .then(data => {
            return (
              typeof data == 'object'
            )
            ? new notifBox(Object.assign({},config,data)) // priorité aux valeurs retour JSON
            : new notifBox(data,config) // data == texte formaté ou message
          })
          .then( () => {enableBtn(el)} )
          ;     
    }
  }


  // transformer les .reponse_formulaire_xxx en notification auto 
  for (let reponse_form of [...document.querySelectorAll(spipConfig.notifbox.selectors.forms)]) {
      reponse_form.dataset.notifboxAuto = '';
      for (let [key, value] of Object.entries({_ok:'success',_erreur:'error'})) {
          let re = new RegExp('.*'+key,"g");
          if([].some.call(reponse_form.classList, c => re.test(c))) {
              reponse_form.dataset.notifboxClass = value;
          }
      }
  }

  // cibler les éléments avec déclenchement automatique dès affichage de la page ou inclure ajaxé
  for (let autofire of [...document.querySelectorAll(spipConfig.notifbox.selectors.auto)]
        .filter( e => !e.classList.contains('hasbox'))
      ) {
      let config = getValidConfigfromAttributes(autofire,'data-notifbox-',spipConfig.notifbox.params);
      if ( !config.hasOwnProperty('message') ) {
        config.message = autofire.dataset.notifboxAuto || autofire.innerHTML;
      };
      if (config.message) {
        new notifBox(config);
        autofire.classList.add('hasbox');
      }
  }

  // surveiller le clic sur les boutons/div statiques, avec data-notifbox-*
  for (let btn_static of [...document.querySelectorAll(spipConfig.notifbox.selectors.btn_static)]
        .filter( e => !e.classList.contains('hasbox'))
      ) {
      let config = getValidConfigfromAttributes(btn_static,'data-notifbox-',spipConfig.notifbox.params);
      if ( !config.hasOwnProperty('message') ) {
        config.message = btn_static.dataset.notifbox || btn_static.innerHTML;
      };
      if (config.message) {
        btn_static.addEventListener('click', (e) => {
          new notifBox(config) 
        });
        btn_static.classList.add('hasbox');
      }
  }

  // surveiller les boutons d'action 
  for (let btn_action of [...document.querySelectorAll(spipConfig.notifbox.selectors.url_action)]
        .filter( e => !e.classList.contains('hasbox'))
    ) {
    btn_action.addEventListener('click', (e) => fetchThenNotif(e));
    btn_action.classList.add('hasbox');
  }
}

// init !
document.addEventListener('DOMContentLoaded',  () => {
  notifbox_init(); 
  if (typeof jQuery == 'function' && typeof onAjaxLoad == 'function') {
    $( () => {onAjaxLoad(notifbox_init)}); 
  }
});