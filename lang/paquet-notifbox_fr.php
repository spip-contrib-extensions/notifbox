<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// N
	'notifbox_description' => 'Simplifie l\'habillage des retours d\'#URL_ACTION, mais pas que. Voir la page de démonstration <em>?exec=notifbox</em>, une fois installé.',
	'notifbox_nom' => 'NotifBox',
	'notifbox_slogan' => 'Gestion des notifications écran dans l\'espace privé',
];
