<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	'bouton_valider' => 'Valider',
// N
	'notif_forms_choix_tous' => 'Activer pour tous les formulaires',
	'notif_forms_choix_desactive' => 'Désactiver cette fonctionnalité',
	'notif_forms_label' => 'Notifications pour les messages retours de formulaires',

// T
	'titre_page_configurer_notifbox' => 'Configuration des notifications',
	'titre_page_demo' => 'Documentation des notifications'
];
