<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Pipeline d'insertion de balise dans le head de la page de l'espace privé
 *
 * @pipeline header_prive
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function notifbox_header_prive($flux) {

	if ($script = find_in_path("javascript/notifbox.js")) {
		$flux .= "<script type=\"text/javascript\" src=\"" . timestamp($script) . "\"></script>\n";
	}
	if (
		boolval(lire_config('notifbox/selectors/forms',0))
	) {
		$flux .= "<script type=\"text/javascript\">\n";
		$flux .= "spipConfig.notifbox.selectors.forms = ' ';\n";
		$flux .= "</script>\n";	
	}
	
	return $flux;
}
