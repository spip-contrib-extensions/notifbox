<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/config');

function formulaires_configurer_notifbox_saisies_dist() {

	$saisies = [

		[
			'saisie' => 'radio',
			'options' => [
				'nom' => 'selectors/forms',
				'label' => _T('notifbox:notif_forms_label'),
				'defaut' => lire_config('notifbox/prive/selectors/forms', '0'),
				'data' => [
					_T('notifbox:notif_forms_choix_tous'),
					_T('notifbox:notif_forms_choix_desactive')
				]
			]
		],
	];

	return $saisies;
}
