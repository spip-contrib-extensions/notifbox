<?php

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

function formulaires_demo_notifbox_charger_dist() {
    $charger = [
        'case_content' => '',
    ];
    return $charger;
}

function formulaires_demo_notifbox_verifier_dist() {
    $erreurs = [];
    if (!_request('case_content')) {
        $erreurs[] = ':(';
    }
    return $erreurs;
}

function formulaires_demo_notifbox_traiter_dist() {
    return [
        'message_ok' => 'Voilà ! merci bien !',
    ];
}
