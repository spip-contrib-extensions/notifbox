<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/notifbox');

function action_demo_notifbox_dist($arg = null) {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();
	list($quoi) = explode('/', $arg);

	switch ($quoi) {
		
		case 'retour_facile_ok':
			return notifbox_retour_ok(['title' => 'Impec !','message' => 'Action réalisée avec succès.']);

		case 'retour_facile_notice':
			return notifbox_retour_notice('Le serveur répond Oups !');
			break;	

		case 'retour_facile_erreur':
			return notifbox_retour_erreur(['title' => 'Aie !','message' => 'Le serveur annonce un échec.']);
			break;				

		case 'styles':
			$retour = recuperer_fond('prive/squelettes/inclure/notifbox/demo_json'); 
			$content_type = 'application/json';
			return ajax_retour($retour,$content_type);
			break;

		case 'jobs':
			$retour = recuperer_fond('prive/squelettes/inclure/notifbox/demo_html');
			$content_type = 'text/html';
			return ajax_retour($retour,$content_type);
			break;

		default: 
			$retour = '<pre>' . print_r(['foo' => 'bar'],1) . '</pre>';
			$content_type = 'text/plain';
			return ajax_retour($retour,$content_type);
			break;

	}

}