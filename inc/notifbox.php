<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function notifbox_json_validate($texte): bool {
	json_decode($texte);
	return json_last_error() === JSON_ERROR_NONE;
}

function notifbox_retour($texte_ou_tab, $type_notif) {

	if (
		$texte_ou_tab
		and is_string($texte_ou_tab)
	) {
		$texte = notifbox_json_validate($texte_ou_tab)
			? json_decode($texte_ou_tab, true)
			: $texte_ou_tab;
		$options = ['message' => $texte];
	}
	elseif (
		is_array($texte_ou_tab)
		and !empty($texte_ou_tab)
	) {
		$options = $texte_ou_tab;
	}
	else {
		spip_log("$texte_ou_tab est vide", 'notifbox_' . _LOG_ERREUR);
		return;
	}

	switch ($type_notif) {
		case 'ok':
			$options['class'] = 'success';
			break;
		case 'notice':
			$options['class'] = 'notice';
			break;
		case 'erreur':
			$options['class'] = 'error';
			$options['duration'] = 60000;
			break;
		default:
			$options['class'] = 'info';
			break;
	}

	return ajax_retour(json_encode($options), 'application/json');
}


function notifbox_retour_log($retour) {
	return  notifbox_retour($retour);
}
function notifbox_retour_ok($retour) {
	return  notifbox_retour($retour, 'ok');
}
function notifbox_retour_notice($retour) {
	return  notifbox_retour($retour, 'notice');
}
function notifbox_retour_erreur($retour) {
	return  notifbox_retour($retour, 'erreur');
}
