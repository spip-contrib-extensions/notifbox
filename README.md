# NOTIFBOX

Ce plugin permet d'afficher des notifications écran pour l'espace privé.
Il s'appuie sur le style des boîtes d'alerte.


## Aperçu
![Notifbox Preview](https://git.spip.net/spip-contrib-extensions/notifbox/raw/branch/main/prive/themes/spip/images/notifbox-preview.gif "Prévisualisation des notifications")


## USAGE
Plusieurs façons de déclencher une notification sont possibles :

### Appels javascript normalisés
```
notifbox.log('Quo vadis ? ');
notifbox.ok('Sic transit gloria mundi');
notifbox.notice('Nota bene');
notifbox.error('Quos vult perdere Jupiter dementat');

```

### Appel javascript avec tableau options
```
new notifBox("Texte du message", {class:'success',title:'Titre du message'});
```

### Appel au clic sur des éléments HTML dotés de l'attribut `data-notifbox`
```
<div
	data-notifbox 
	data-notifbox-timer 
	data-notifbox-class="basic hightlight" 
	data-notifbox-duration="15000" >
Lorem ipsum dolor sit amet, consectetur adipisicing elit.
</div>
``` 

### Interception automatique des `.reponse_formulaire`

Option désactivable

### Traitement simplifié pour le retour des liens #URL_ACTION
```
  <a class="action-notifbox btn" 
     href="#URL_ACTION_AUTEUR{action,arg}">
     Action
  </a>
```

_____

Une fois installé, consultez la page démonstration : `ecrire/?exec=notifbox`